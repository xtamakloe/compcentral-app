package com.artoconnect.compcentral.event;

import com.artoconnect.compcentral.model.Category;

/**
 * Created by Christian Tamakloe on 09/09/15.
 */
public class SelectCategoryEvent {
    public final Category mCategory;

    public SelectCategoryEvent(Category mCategory) {
        this.mCategory = mCategory;
    }
}
