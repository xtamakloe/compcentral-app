package com.artoconnect.compcentral.event;

import com.artoconnect.compcentral.model.Video;

/**
 * Created by Christian Tamakloe on 07/09/15.
 */
public class SaveVideoEvent {
    public final Video mVideo;

    public SaveVideoEvent(Video video) {
        this.mVideo = video;
    }
}