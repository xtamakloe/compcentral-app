package com.artoconnect.compcentral.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.artoconnect.compcentral.fragment.CategoriesFragment;
import com.artoconnect.compcentral.fragment.SavedVideoListFragment;
import com.artoconnect.compcentral.fragment.VideoListFragment;

/**
 * Created by Christian Tamakloe on 02/09/15.
 */
public class HomeFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private static final int TAB_COUNT = 4;

    public HomeFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return CategoriesFragment.newInstance();

            case 1:
                return VideoListFragment.newInstance("funny video");

            case 2:
                return SavedVideoListFragment.newInstance(SavedVideoListFragment.FAVOURITES);

            case 3:
                return SavedVideoListFragment.newInstance(SavedVideoListFragment.WATCH_LATER);

            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

}
