package com.artoconnect.compcentral.fragment;


import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artoconnect.compcentral.R;
import com.artoconnect.compcentral.adapter.HomeFragmentPagerAdapter;
import com.artoconnect.compcentral.event.SelectCategoryEvent;
import com.mikepenz.google_material_typeface_library.GoogleMaterial.Icon;
import com.mikepenz.iconics.IconicsDrawable;

import de.greenrobot.event.EventBus;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private Integer[] mTabTitles = {
            R.string.popular,
            R.string.title_browse,
            R.string.title_favourites,
            R.string.title_watch_later
    };
    private Icon[] mTabIcons = {
            Icon.gmd_trending_up,
            Icon.gmd_video_library,
            Icon.gmd_favorite,
            Icon.gmd_alarm
    };
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private String mCurrentPageTwoTitle;


    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        final HomeFragmentPagerAdapter mPagerAdapter =
                new HomeFragmentPagerAdapter(getActivity(), getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.vpPager);
        mTabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        mCurrentPageTwoTitle = getResources().getString(R.string.title_browse); // Set initial title for page 2

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(0); // added
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mViewPager.setCurrentItem(position);

                mTabLayout.getTabAt(position).select(); // Set current tab
                setHighlight(mTabLayout.getTabAt(position)); // Highlight current tab

                // Set title based on current page. Except for page 2, which is set based on
                // category selected in page 1, after it has been initially set
                String title =
                        (position == 1) ? mCurrentPageTwoTitle : getActivity().getResources().getString(mTabTitles[position]);

                setTitle(title);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        for (Icon icon : mTabIcons) {
            mTabLayout.addTab(mTabLayout.newTab().setIcon(
                    new IconicsDrawable(getContext())
                            .icon(icon)
                            .color(R.color.primary_light)
                            .sizeDp(24)));
        }
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                // Highlight current tab
                setHighlight(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        setHighlight(mTabLayout.getTabAt(0));
        return view;
    }

    void setHighlight(TabLayout.Tab tab) {
        for (int i = 0; i < 3; i++) {
            mTabLayout.getTabAt(i).getIcon().clearColorFilter();
        }
        tab.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }


    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(SelectCategoryEvent event) {
        mViewPager.setCurrentItem(1);
        mCurrentPageTwoTitle = event.mCategory.getTitle();
        setTitle(mCurrentPageTwoTitle);
    }

    public void setTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }
}
