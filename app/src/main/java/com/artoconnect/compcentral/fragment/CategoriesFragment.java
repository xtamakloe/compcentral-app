package com.artoconnect.compcentral.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.compcentral.R;
import com.artoconnect.compcentral.adapter.CategoryListAdapter;
import com.artoconnect.compcentral.helper.APIService;
import com.artoconnect.compcentral.helper.SimpleDividerItemDecoration;
import com.artoconnect.compcentral.model.Category;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private ProgressBar mProgressbar;
    private RelativeLayout mErrorView;
    private List<Category> mCategories;
    private CategoryListAdapter mAdapter;

    public CategoriesFragment() {
    }

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);

        // List
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvCategories);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        // Progress bar
        mProgressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);

        // Set up and hide empty view
        mErrorView = (RelativeLayout) rootView.findViewById(R.id.rlErrorView);
        mErrorView.setVisibility(View.GONE);

        // Retry button
        rootView.findViewById(R.id.btnRetry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadCategories();
            }
        });

        loadCategories();

        return rootView;
    }

    private void loadCategories() {
        mRecyclerView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.GONE);
        mProgressbar.setVisibility(View.VISIBLE);

        mCategories = Category.getCategories();
        if (mCategories.size() > 0) {
            mAdapter = new CategoryListAdapter(mCategories);
            mRecyclerView.setAdapter(mAdapter);

            mProgressbar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            // Get latest categories and update view
            APIService.getClient().getCategories().enqueue(new Callback<List<Category>>() {
                @Override
                public void onResponse(Response<List<Category>> response) {
                    mCategories = response.body();
                    mAdapter = new CategoryListAdapter(mCategories);
                    mRecyclerView.setAdapter(mAdapter);

                    // Persist
                    for (Category category : mCategories) {
                        category.save();
                    }

                    mProgressbar.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Throwable t) {
                    mProgressbar.setVisibility(View.GONE);
                    mErrorView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

}
