package com.artoconnect.compcentral;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Christian Tamakloe on 03/09/15.
 */
public class CompCentralApplication extends com.activeandroid.app.Application {
    public static final String LOG_TAG = "---XXCmPCENtralXX---";
    public static final String TD_NOTE_3 = "145FBB0183F9754F20261F929E0CE7E3";
    public static final String TD_DUOS = "375C8EE279DC1546120035A3507E2FB0";


    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Lato/Lato-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
