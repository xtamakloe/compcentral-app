package com.artoconnect.compcentral.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.artoconnect.compcentral.BuildConfig;
import com.artoconnect.compcentral.R;

public class SplashScreenActivity extends AppCompatActivity {
    private static final int SPLASH_DISPLAY_TIME = 550;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ((TextView) findViewById(R.id.tv_version)).setText(BuildConfig.VERSION_NAME);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(SplashScreenActivity.this, MainActivity.class);

                SplashScreenActivity.this.startActivity(intent);
                SplashScreenActivity.this.finish();
            }

        }, SPLASH_DISPLAY_TIME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
