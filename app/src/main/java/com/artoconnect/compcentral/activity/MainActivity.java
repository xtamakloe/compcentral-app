package com.artoconnect.compcentral.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.artoconnect.compcentral.CompCentralApplication;
import com.artoconnect.compcentral.R;
import com.artoconnect.compcentral.fragment.HomeFragment;
import com.artoconnect.compcentral.helper.RateThisApp;
import com.artoconnect.compcentral.model.Category;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends BaseActivity {

    private Toolbar mToolbar;
    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(CompCentralApplication.TD_DUOS)
//                .addTestDevice(CompCentralApplication.TD_NOTE_3)
                .build();
        mAdView.loadAd(adRequest);

        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        mToolbar.setTitle(getResources().getString(R.string.popular));
        setSupportActionBar(mToolbar);

        mFragment = HomeFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mFragment, "mFragment")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_instructions:
//                startActivity(new Intent(this, InstructionsActivity.class));
                return true;

            case R.id.action_share_app:
                // Tell friends
                Intent shareIntent = new Intent(Intent.ACTION_SEND).setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_message));
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_via)));
                return true;

            case R.id.action_rate_app:
                // Rate app
                RateThisApp.showRateDialog(this);
                return true;

            case R.id.action_support:
                // Contact Support
                Intent Email = new Intent(Intent.ACTION_SEND);
                Email.setType("message/rfc822");
                Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@artoconnect.com"});
                Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback for CompCentral");
                Email.putExtra(Intent.EXTRA_TEXT, "Dear " + "");
                startActivity(Intent.createChooser(Email, "We'd love to hear from you!"));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Category.clearCategories();
    }

    @Override
    protected void onStart() {
        super.onStart();
        RateThisApp.onStart(this);
        RateThisApp.showRateDialogIfNeeded(this);
    }
}
