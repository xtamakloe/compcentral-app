package com.artoconnect.compcentral.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.artoconnect.compcentral.CompCentralApplication;
import com.artoconnect.compcentral.R;
import com.artoconnect.compcentral.adapter.VideoListAdapter;
import com.artoconnect.compcentral.event.SaveVideoEvent;
import com.artoconnect.compcentral.model.Video;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

import de.greenrobot.event.EventBus;

public class VideoActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    FrameLayout.LayoutParams COVER_SCREEN_GRAVITY_CENTER = new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
    private WebView mWebView;
    private RelativeLayout mContentView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private WebChromeClient mWebChromeClient;
    private View mCustomView;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(CompCentralApplication.TD_DUOS)
//                .addTestDevice(CompCentralApplication.TD_NOTE_3)
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(
                getResources().getString(R.string.interstitial_ad_unit_webview));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                finish();
            }
        });
        requestNewInterstitial();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mContentView = (RelativeLayout) findViewById(R.id.relativelayout);
        mWebView = (WebView) findViewById(R.id.webView);
        mCustomViewContainer = (FrameLayout) findViewById(R.id.fullscreen_custom_content);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);

        mToolbar.setTitle(getResources().getString(R.string.app_name));
        mToolbar.inflateMenu(R.menu.menu_video);
        mToolbar.setOnMenuItemClickListener(this);
        mToolbar.setNavigationIcon(R.mipmap.ic_arrow_left_white_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    /*
                    // 50/50 chance of showing ad
                    if (new Random().nextInt(2) == 0) {
                        mInterstitialAd.show();
                        finish();
                    }*/
                    finish();
                } else {
                    finish();
                }
            }
        });

        mWebChromeClient = new WebChromeClient() {

            @Override
            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                // if a view already exists then immediately terminate the new one
                if (mCustomView != null) {
                    callback.onCustomViewHidden();
                    return;
                }
                // Add the custom view to its container.
                mCustomViewContainer.addView(view, COVER_SCREEN_GRAVITY_CENTER);
                mCustomView = view;
                mCustomViewCallback = callback;

                // hide main browser view
                mContentView.setVisibility(View.GONE);

                // Finally show the custom view container.
                mCustomViewContainer.setVisibility(View.VISIBLE);
                mCustomViewContainer.bringToFront();
            }

            @Override
            public void onHideCustomView() {
                if (mCustomView == null)
                    return;

                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);
                // Remove the custom view from its container.
                mCustomViewContainer.removeView(mCustomView);
                mCustomView = null;
                mCustomViewContainer.setVisibility(View.GONE);
                mCustomViewCallback.onCustomViewHidden();

                // Show the content view.
                mContentView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        };

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        // get url from intent arguments
        mWebView.loadUrl(getIntent().getStringExtra(VideoListAdapter.EXTRA_URL));
        mWebView.setWebViewClient(new CustomWebViewClient());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCustomView != null) {
            if (mCustomViewCallback != null)
                mCustomViewCallback.onCustomViewHidden();
            mCustomView = null;
        }
        if (mWebView != null)
            mWebView.destroy();

        EventBus.getDefault().post(new SaveVideoEvent(null)); // Hack to refresh saved video fragments
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mCustomView != null) {
            mWebChromeClient.onHideCustomView();
        } else {
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                Video.share(mWebView.getUrl(), this);
                return true;

            case R.id.action_favourite:
                Video.saveFromUrl(mWebView.getUrl(), Video.FAVOURITE, this);
                return true;

            case R.id.action_watch_later:
                Video.saveFromUrl(mWebView.getUrl(), Video.WATCH_LATER, this);
                return true;

            default:
                return false;
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice(CompCentralApplication.TD_DUOS)
//                .addTestDevice(CompCentralApplication.TD_NOTE_3)
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url) {
            webview.setWebChromeClient(mWebChromeClient);
            webview.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            VideoActivity.this.mProgressBar.setProgress(100);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            VideoActivity.this.mProgressBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }
    }
}
