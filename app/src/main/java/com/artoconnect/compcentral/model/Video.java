package com.artoconnect.compcentral.model;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.artoconnect.compcentral.R;
import com.artoconnect.compcentral.helper.Util;
import com.artoconnect.compcentral.helper.YoutubeConnector;

import java.util.List;

/**
 * Created by Christian Tamakloe on 17/07/15.
 */
@Table(name = "videos")
public class Video extends Model {

    public static final String FAVOURITE = "favourite";
    public static final String WATCH_LATER = "watch_later";


    @Column(name = "uid", index = true)
    private String uid;

    @Column(name = "title")
    private String title;

    @Column(name = "url")
    private String url;

    @Column(name = "thumbnail_url")
    private String thumbnailUrl;

    @Column(name = "favourite")
    private boolean isFavourite;

    @Column(name = "watch_later")
    private boolean watchLater;

    public Video() {

    }

    public Video(String title, String thumbnailUrl, String uid, String url, boolean isFavourite, boolean watchLater) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.uid = uid;
        this.url = url;
        this.isFavourite = isFavourite;
        this.watchLater = watchLater;
    }

    public static void watchLater(Video video) {
        Video v = new Select()
                .from(Video.class)
                .where("uid = ?", video.getUid())
                .executeSingle();
        if (v == null) {
            v = new Video(video.getTitle(),
                    video.getThumbnailUrl(),
                    video.getUid(),
                    video.getUrl(),
                    false,
                    true);
        } else {
            v.setWatchLater(true);
        }
        v.save();
    }

    public static void addToFavourites(Video video) {
        Video v = new Select()
                .from(Video.class)
                .where("uid = ?", video.getUid())
                .executeSingle();
        if (v == null) {
            v = new Video(video.getTitle(),
                    video.getThumbnailUrl(),
                    video.getUid(),
                    video.getUrl(),
                    true,
                    false);
        } else {
            v.setIsFavourite(true);
        }
        v.save();
    }

    public static void saveFromUrl(final String url, final String saveAs, final Context context) {
        retrieveVideoInfoFromYoutube(url, saveAs, context);
    }

    public static void share(String url, Context context) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND).setType("text/plain");
        String shareMessage = "Check out this vid I found using CompCentral - "
                + url
                + " ... Get the app here - "
                + context.getResources().getString(R.string.app_bitly_link);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        context.startActivity(Intent.createChooser(shareIntent,
                context.getString(R.string.share_via)));
    }

    public static List<Video> getFavourites() {
        return new Select().from(Video.class)
                .orderBy("title ASC")
                .where("favourite = 1")
                .execute();
    }

    public static List<Video> getWatchList() {
        return new Select().from(Video.class)
                .orderBy("title ASC")
                .where("watch_later = 1")
                .execute();
    }

    public static void remove(Video video) {
        new Delete().from(Video.class)
                .where("uid = ?", video.getUid())
                .execute();
    }

    static void retrieveVideoInfoFromYoutube(final String url, final String saveAs, final Context context) {
        new Thread() {
            public void run() {
                String videoId = null;
                final View view = ((AppCompatActivity) context).findViewById(android.R.id.content);
                String[] urlSegments = url.split("\\?");
                if (urlSegments.length > 1 && urlSegments[1].startsWith("v")) {
                    videoId = urlSegments[1].replace("v=", "");
                    // Show progress message
                    Snackbar.make(view,
                            view.getResources().getString(R.string.notification_getting_video),
                            Snackbar.LENGTH_SHORT).show();
                    // Retrieve video
                    YoutubeConnector yc = new YoutubeConnector(context);
                    final List<Video> searchResults = yc.search(videoId);
                    new Handler(Looper.getMainLooper()).post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    int messageId = 0;
                                    if (searchResults != null) {
                                        Video video = searchResults.get(0);

                                        if (saveAs.equals(Video.FAVOURITE)) {
                                            Video.addToFavourites(video);
                                            messageId = R.string.notification_added_favourites;
                                        } else if (saveAs.equals(Video.WATCH_LATER)) {
                                            Video.watchLater(video);
                                            messageId = R.string.notification_added_watch_list;
                                        }
                                    } else {
                                        messageId = R.string.notification_getting_video_failed;
                                    }
                                    Snackbar.make(view,
                                            view.getResources().getText(messageId),
                                            Snackbar.LENGTH_LONG).show();
                                }
                            }
                    );
                } else {
                    Snackbar.make(view,
                            view.getResources().getString(R.string.notification_invalid_url),
                            Snackbar.LENGTH_LONG).show();
                }
            }
        }.start();
    }

    public void setWatchLater(boolean watchLater) {
        this.watchLater = watchLater;
    }

    public void setIsFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return Util.titleize(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
